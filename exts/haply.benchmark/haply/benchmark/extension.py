import omni.ext
import omni.ui as ui
import time

from pxr import Usd, Gf, Sdf
from pxr import UsdGeom, UsdUtils, UsdPhysics
from pxr import PhysxSchema
# Any class derived from `omni.ext.IExt` in top level module (defined in `python.modules` of `extension.toml`) will be
# instantiated when extension gets enabled and `on_startup(ext_id)` will be called. Later when extension gets disabled
# on_shutdown() is called.


class MyExtension(omni.ext.IExt):
    # ext_id is current extension id. It can be used with extension manager to query additional information, like where
    # this extension is located on filesystem.
    def on_startup(self, ext_id):
        print("[haply.benchmark] MyExtension startup")

        self._window = ui.Window("Physics SpeedTest", width=200, height=50)
        with self._window.frame:
            with ui.HStack():
                ui.Label("Physics SpeedTest: ")
                self.results = ui.Label("")

        physxInterface = omni.physx.get_physx_interface()
        self.subscription_handle = physxInterface.subscribe_physics_step_events(
            self.on_update)
        self.oldtime = time.perf_counter()
        self.countdt = 0

    def on_update(self, dt):
        self.countdt += 1
        if self.countdt > 100:
            self.countdt = 0
            freq = 100 / (time.perf_counter() - self.oldtime)
            self.oldtime = time.perf_counter()
            #print(round(freq, 2))
            self.results.text = str(round(freq, 2)) + " Hz"
        # print("dt")

    def on_shutdown(self):
        print("[haply.benchmark] MyExtension shutdown")
